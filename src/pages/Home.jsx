import "./App.css";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import { BsStarFill } from "react-icons/bs";
import {
  Navbar,
  Container,
  Button,
  Nav,
  Row,
  Col,
  Card,
  Accordion,
} from "react-bootstrap";
import CarsLogo from "../public/img/img_car.png";
import CheckLogo from "../public/img/Group_53.png";
import ServiceImg from "../public/img/img_service.png";
import CompleteIcon from "../public/img/icon_complete.png";
import PriceIcon from "../public/img/icon_price.png";
import TimeIcon from "../public/img/icon_24hrs.png";
import ProIcon from "../public/img/icon_professional.png";
import People1 from "../public/img/img_photo.png";
import People2 from "../public/img/img_photo_(1).png";
import People3 from "../public/img/photo_2.jpg";
import Facebook from "../public/img/icon_facebook.png";
import Instagram from "../public/img/icon_instagram.png";
import Twitter from "../public/img/icon_twitter.png";
import Mail from "../public/img/icon_mail.png";
import Twitch from "../public/img/icon_twitch.png";
import { useSelector } from "react-redux";
import { selectUser } from "../slices/userSlice";

function Home() {
  const navigate = useNavigate();
  const userRedux = useSelector(selectUser);
  const [user, setUser] = useState(userRedux.creds);
  const styleButton = {
    backgroundColor: "#5cb85f",
    borderRadius: "0px",
  };

  const navStyle = {
    padding: "8px 10px",
  };

  const iconColor = {
    color: "gold",
  };

  const styleP = {
    fontSize: "14px",
  };

  const onClickLogin = () => {
    navigate("/filter");
  };

  return (
    <>
      <Navbar fixed="top" className="navbar">
        <Container>
          <Navbar.Brand href="#home" className="box">
            {user.name}
          </Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end text-decoration-none ">
            <Nav.Link href="#service" className="text-black" style={navStyle}>
              Our Service
            </Nav.Link>
            <Nav.Link href="#why-us" className="text-black" style={navStyle}>
              Why Us
            </Nav.Link>
            <Nav.Link
              href="#testimonial"
              className="text-black"
              style={navStyle}
            >
              Testimonial
            </Nav.Link>
            <Nav.Link href="#faq" className="text-black" style={navStyle}>
              FAQ
            </Nav.Link>
            <Nav.Link href="#" className="text-black" style={navStyle}>
              <Link to="/register">
                <Button variant="success" style={styleButton}>
                  Register
                </Button>
              </Link>
            </Nav.Link>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <section className="hero">
        <Container>
          <Row>
            <Col md={6} className="hero-text">
              <p className="title">
                Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
              </p>
              <p className="content">
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>

              <Button
                variant="success"
                style={styleButton}
                onClick={(e) => onClickLogin(e)}
              >
                Mulai Sewa Mobil
              </Button>
            </Col>
            <Col md={6} className="picture">
              <img src={CarsLogo} alt="llog of cars" />
            </Col>
          </Row>
        </Container>
      </section>

      <section className="service" id="service">
        <Container>
          <Row className="our-service">
            <Col md={6} className="img-service">
              <img src={ServiceImg} className="img-fluid" alt="images" />
            </Col>
            <Col md={6} className="text-service">
              <h3 className="fw-bold">
                Best Car Rental for any kind of trip in (Lokasimu)!
              </h3>
              <p className="pt-3">
                Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
                lebih murah dibandingkan yang lain, kondisi mobil baru, serta
                kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
                wedding, meeting, dll.
              </p>
              <p>
                <img src={CheckLogo} alt="images" />
                <span className="ms-3">
                  Sewa Mobil Dengan Supir di Bali 12 Jam
                </span>
              </p>
              <p>
                <img src={CheckLogo} alt="images" />
                <span className="ms-3">
                  Sewa Mobil Lepas Kunci di Bali 24 Jam
                </span>
              </p>
              <p>
                <img src={CheckLogo} alt="images" />
                <span className="ms-3">Sewa Mobil Jangka Panjang Bulanan</span>
              </p>
              <p>
                <img src={CheckLogo} alt="images" />
                <span className="ms-3">
                  Gratis Antar - Jemput Mobil di Bandara
                </span>
              </p>
              <p>
                <img src={CheckLogo} alt="images" />
                <span className="ms-3">
                  Layanan Airport Transfer / Drop In Out
                </span>
              </p>
            </Col>
          </Row>
        </Container>
      </section>

      <section className="why-us" id="why-us">
        <Container>
          <div className="why-title">
            <h3 className=" fw-bold">Why Us?</h3>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>

          <div className="why-text d-flex justify-content-center gap-4">
            <Card style={{ width: "18rem" }}>
              <Card.Body>
                <Card.Title>
                  <img src={CompleteIcon} alt="" />
                </Card.Title>
                <Card.Subtitle className="my-3 fw-bold ">
                  Mobil Lengkap
                </Card.Subtitle>
                <Card.Text className="card-text">
                  Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                  terawat
                </Card.Text>
              </Card.Body>
            </Card>

            <Card style={{ width: "18rem" }}>
              <Card.Body>
                <Card.Title>
                  <img src={PriceIcon} alt="" />
                </Card.Title>
                <Card.Subtitle className="my-3 fw-bold ">
                  Harga Murah
                </Card.Subtitle>
                <Card.Text className="card-text">
                  Harga murah dan bersaing, bisa bandingkan harga kami dengan
                  rental mobil lain
                </Card.Text>
              </Card.Body>
            </Card>

            <Card style={{ width: "18rem" }}>
              <Card.Body>
                <Card.Title>
                  <img src={TimeIcon} alt="" />
                </Card.Title>
                <Card.Subtitle className="my-3 fw-bold ">
                  Layanan 24 Jam
                </Card.Subtitle>
                <Card.Text className="card-text">
                  Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                  tersedia di akhir minggu
                </Card.Text>
              </Card.Body>
            </Card>

            <Card style={{ width: "18rem" }}>
              <Card.Body>
                <Card.Title>
                  <img src={ProIcon} alt="" />
                </Card.Title>
                <Card.Subtitle className="my-3 fw-bold ">
                  Sopir Profesional
                </Card.Subtitle>
                <Card.Text className="card-text">
                  Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                  tepat waktu
                </Card.Text>
              </Card.Body>
            </Card>
          </div>
        </Container>
      </section>

      <section className="testimonial" id="testimonial">
        <div className="Testimonial-title text-center" id="Testimonial">
          <h2 className=" fw-bold">Testimonial</h2>
          <p>Berbagai review positif dari para pelanggan kami</p>
        </div>

        <OwlCarousel
          items={2}
          className="owl-theme"
          loop
          nav
          center
          margin={10}
        >
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img
                      src={People2}
                      className="rounded-circle"
                      alt="images"
                    />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon" style={iconColor}>
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img
                      src={People1}
                      className="rounded-circle"
                      alt="images"
                    />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon" style={iconColor}>
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="item">
                <div className="testi-body">
                  <div className="card-left img-card">
                    <img
                      src={People3}
                      className="rounded-circle"
                      alt="images"
                    />
                  </div>
                  <div className="card-right">
                    <div className="faq-icon" style={iconColor}>
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                      <BsStarFill />
                    </div>
                    <p>
                      “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                      do eiusmod lorem ipsum dolor sit amet, consectetur
                      adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                      amet, consectetur adipiscing elit, sed do eiusmod”
                    </p>
                    <p class="fw-bold">John Dee 32, Bromo</p>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </OwlCarousel>
      </section>

      <section className="banner">
        <Container>
          <div className="banner-content text-center text-white">
            <h3 className=" fw-bold">Sewa Mobil di (Lokasimu) Sekarang</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
            </p>

            <Button
              variant="success"
              onClick={(e) => onClickLogin(e)}
              style={styleButton}
            >
              Mulai Sewa Mobil
            </Button>
          </div>
        </Container>
      </section>

      <section className="faq" id="faq">
        <Container>
          <Row>
            <Col md={5} className="faq-title">
              <h1 className="fw-bold">Frequently Asked Question</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </Col>
            <Col md={7}>
              <Accordion className="faq-content">
                <Card className="faq-card">
                  <Accordion.Item eventKey="0">
                    <Accordion.Header>
                      Apa saja syarat yang dibutuhkan
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="1">
                    <Accordion.Header>
                      Berapa hari minimal sewa mobil lepas kunci?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="2">
                    <Accordion.Header>
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="3">
                    <Accordion.Header>
                      Apakah Ada biaya antar-jemput?
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>

                <Card className="faq-card">
                  <Accordion.Item eventKey="4">
                    <Accordion.Header>
                      Bagaimana jika terjadi kecelakaan
                    </Accordion.Header>
                    <Accordion.Body className="faq-p">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                      Distinctio tempora ad quibusdam necessitatibus, quod
                      blanditiis aliquam dolorum illo vel omnis quam? Nam
                      exercitationem vitae odit sapiente, ab impedit sint
                      praesentium?
                    </Accordion.Body>
                  </Accordion.Item>
                </Card>
              </Accordion>
            </Col>
          </Row>
        </Container>
      </section>

      <section className="footer">
        <Container>
          <footer>
            <Row>
              <Col md={3} style={styleP}>
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
              </Col>
              <Col md={2} style={styleP} className="footer-link">
                <p>
                  <a href="#our-service">Our Service</a>
                </p>
                <p>
                  <a href="#why-us">Why Us</a>
                </p>
                <p>
                  <a href="#testimonial">Testimonial</a>
                </p>
                <p>
                  <a href="#faq">FAQ</a>
                </p>
              </Col>
              <Col md={3}>
                <p style={styleP}>Connect with us</p>
                <p className="icons" style={styleP}>
                  <img src={Facebook} alt="images" />
                  <img src={Instagram} alt="images" />
                  <img src={Twitter} alt="images" />
                  <img src={Mail} alt="images" />
                  <img src={Twitch} alt="images" />
                </p>
              </Col>
              <Col md={4}>
                <p style={styleP}>Copyright Binar 2022</p>
                <Col className="box"></Col>
              </Col>
            </Row>
          </footer>
        </Container>
      </section>
    </>
  );
}

export default Home;
